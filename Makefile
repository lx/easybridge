BIN=easybridge
DOCKER=lxpz/easybridge_amd64

SRC= mxlib/registration.go mxlib/api.go mxlib/mediaobject.go mxlib/client.go \
	connector/connector.go connector/config.go \
	connector/mediaobject.go connector/marshal.go \
	connector/irc/irc.go connector/irc/config.go \
	connector/xmpp/config.go connector/xmpp/xmpp.go \
	connector/mattermost/mattermost.go connector/mattermost/config.go \
	connector/external/external.go connector/external/config.go \
	web.go account.go main.go server.go db.go util.go

all: $(BIN)

$(BIN): $(SRC)
	go get -d -v
	go build -v -o $(BIN)

docker: $(BIN)
	docker build -t $(DOCKER):$(TAG) .
	docker push $(DOCKER):$(TAG)
	docker tag $(DOCKER):$(TAG) $(DOCKER):latest
	docker push $(DOCKER):latest

docker_clean: $(BIN)
	docker build --no-cache -t $(DOCKER):$(TAG) .
	docker push $(DOCKER):$(TAG)
	docker tag $(DOCKER):$(TAG) $(DOCKER):latest
	docker push $(DOCKER):latest
