package mxlib

import (
	_ "gopkg.in/yaml.v2"
)

type Registration struct {
	Id              string                   `yaml:"id"`
	Url             string                   `yaml:"url"`
	AsToken         string                   `yaml:"as_token"`
	HsToken         string                   `yaml:"hs_token"`
	SenderLocalpart string                   `yaml:"sender_localpart"`
	RateLimited     bool                     `yaml:"rate_limited"`
	Namespaces      RegistrationNamespaceSet `yaml:"namespaces"`
}

type RegistrationNamespaceSet struct {
	Users   []RegistrationNamespace `yaml:"users"`
	Aliases []RegistrationNamespace `yaml:"aliases"`
	Rooms   []RegistrationNamespace `yaml:"rooms"`
}

type RegistrationNamespace struct {
	Exclusive bool   `yaml:"exclusive"`
	Regex     string `yaml:"regex"`
}
