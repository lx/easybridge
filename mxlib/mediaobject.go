package mxlib

import (
	"fmt"
	"io"
	"net/http"
	"net/url"

	"git.deuxfleurs.fr/Deuxfleurs/easybridge/connector"
)

type MediaObject struct {
	mxClient   *Client
	filename   string
	size       int64
	mimetype   string
	imageSize  *connector.ImageSize
	MxcServer  string
	MxcMediaId string
}

func (m *MediaObject) Filename() string {
	return m.filename
}

func (m *MediaObject) Size() int64 {
	return m.size
}

func (m *MediaObject) Mimetype() string {
	return m.mimetype
}

func (m *MediaObject) ImageSize() *connector.ImageSize {
	return m.imageSize
}

func (m *MediaObject) Read() (io.ReadCloser, error) {
	req, err := http.NewRequest("GET", m.URL(), nil)
	if err != nil {
		return nil, err
	}

	req.Header.Add("Authorization", "Bearer "+m.mxClient.Token)

	resp, err := m.mxClient.httpClient.Do(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("HTTP error %d", resp.StatusCode)
	}

	return resp.Body, nil
}

func (m *MediaObject) URL() string {
	return fmt.Sprintf("%s/_matrix/media/r0/download/%s/%s/%s",
		m.mxClient.Server, m.MxcServer, m.MxcMediaId, url.QueryEscape(m.filename))
}

func (m *MediaObject) MxcUri() string {
	return fmt.Sprintf("mxc://%s/%s", m.MxcServer, m.MxcMediaId)
}
