package mxlib

import (
	_ "encoding/json"
)

type MxError struct {
	ErrCode string `json:"errcode"`
	ErrMsg  string `json:"error"`
}

func (e *MxError) Error() string {
	return e.ErrMsg
}

type Transaction struct {
	Events []Event `json:"events"`
}

type Event struct {
	Content        map[string]interface{} `json:"content"`
	Type           string                 `json:"type"`
	EventId        string                 `json:"event_id"`
	RoomId         string                 `json:"room_id"`
	Sender         string                 `json:"sender"`
	OriginServerTs int                    `json:"origin_server_ts"`
}

type PasswordLoginRequest struct {
	Type                     string            `json:"type"`
	Identifier               map[string]string `json:"identifier"`
	Password                 string            `json:"password"`
	DeviceID                 string            `json:"device_id"`
	InitialDeviceDisplayNAme string            `json:"initial_device_display_name"`
}

type LoginResponse struct {
	UserID      string `json:"user_id"`
	AccessToken string `json:"access_token"`
}

type RegisterRequest struct {
	Auth     RegisterRequestAuth `json:"auth"`
	Type     string              `json:"type"`
	Username string              `json:"username"`
}

type RegisterRequestAuth struct {
	Type string `json:"type"`
}

type RegisterResponse struct {
	UserId      string `json:"user_id"`
	AccessToken string `json:"access_token"`
	DeviceId    string `json:"device_id"`
}

type ProfileDisplaynameRequest struct {
	Displayname string `json:"displayname"`
}

type CreateRoomRequest struct {
	Preset          string                 `json:"preset"`
	RoomAliasName   string                 `json:"room_alias_name"`
	Name            string                 `json:"name"`
	Topic           string                 `json:"topic"`
	Invite          []string               `json:"invite"`
	CreationContent map[string]interface{} `json:"creation_content"`
	PowerLevels     map[string]interface{} `json:"power_level_content_override"`
}

type CreateDirectRoomRequest struct {
	Preset          string                 `json:"preset"`
	Topic           string                 `json:"topic"`
	Invite          []string               `json:"invite"`
	CreationContent map[string]interface{} `json:"creation_content"`
	PowerLevels     map[string]interface{} `json:"power_level_content_override"`
	IsDirect        bool                   `json:"is_direct"`
}

type CreateRoomResponse struct {
	RoomId string `json:"room_id"`
}

type DirectoryRoomResponse struct {
	RoomId  string   `json:"room_id"`
	Servers []string `json:"string"`
}

type RoomInviteRequest struct {
	UserId string `json:"user_id"`
}

type RoomKickRequest struct {
	UserId string `json:"user_id"`
	Reason string `json:"reason"`
}
type RoomJoinResponse struct {
	RoomId string `json:"room_id"`
}

type RoomSendResponse struct {
	EventId string `json:"event_id"`
}

type UploadResponse struct {
	ContentUri string `json:"content_uri"`
}

type ProfileAvatarUrl struct {
	AvatarUrl string `json:"avatar_url"`
}
