FROM archlinux:latest
RUN pacman -Sy python-pip --noconfirm; pacman -Scc --noconfirm; find /var/cache/pacman/ -type f -delete; find /var/lib/pacman/sync/ -type f -delete

#FROM python:3.8.6-buster

RUN pip install fbchat==1.9.7

RUN mkdir /app
WORKDIR /app
ADD static /app/static
ADD easybridge.jpg /app/easybridge.jpg
ADD external /app/external
ADD easybridge /app/easybridge
ADD templates /app/templates

CMD "/app/easybridge"
