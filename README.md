# Easybridge

Easybridge is a Matrix-to-everything (almost) bridge.
It acts as a client under your name on all instant messaging networks where
you have an account, and presents your private messages as well as group
conversations in Matrix in a uniform view.
Basically, Matrix becomes your everything chat client!
Easybridge is a multi-user bridge for small Matrix servers,
with the idea of making it easy for non-technical users to bridge their external accounts into Matrix.
Once configured next to a server,
users can just go to a dedicated web page and add their accounts with a simple form.

**WARNING** Easybridge is still very experimental and crashes sometimes.

Current protocol status:

- IRC: text messages only (private messages and public channels). Has bugs, most notably: messages will be duplicated if several users connect to the same channel.
- XMPP: text messages only (private chat and MUCs), no backlog, no avatars, no file transfer.
- Mattermost: in quite good shape. Private & group conversations with text messages and attachments (images or other files). Handles retrieving of message backlog, user avatars and room avatars from the server.
- Facebook messenger: in quite good shape, handles private messages and groups, text messages, attachments, stickers, profile pictures (low res only), backlog. Sometimes disconnects from the server and messages stop arriving.

Adding a backend shouldn't be too hard if a Golang library exists to connect to that protocol.
Easybridge can also spawn an external process to communicate using a certain protocol if no Golang library
is available (this is currently used by the Facebook Messenger backend which is written in Python using the `fbchat` library).

Current features:

- Handles private chats (one-to-one conversations) as well as group chats (sometimes called channels, multi-user chats, or chat rooms)
- Automatic setup of Matrix rooms that bridge to outside rooms
- Room name and topic synchronization (partially)
- Images and file transfers (Mattermost and FB Messenger backends)
- Avatar and room pictures (Mattermost backend and FB Messenger backends, one-way only)
- Retrieving old messages / missed messages when backend was disconnected (Mattermost and FB Messenger)
- Web interface for setting up accounts so that new accounts can be easily
  added and you don't have to type your credentials in a clear-text Matrix room
- Credentials are stored encrypted in the database using users' Matrix passwords

There is lot to do! See the issues if you want to join us on this project.

A Docker image is provided on the [Docker hub](https://hub.docker.com/r/lxpz/easybridge_amd64).

Easybridge is licensed under the terms of the GPLv3.

Contact me if you have questions or are interested in contributing to the project: [@lx:deuxfleurs.fr](https://matrix.to/#/@lx:deuxfleurs.fr).


## Building Easybridge

Easybridge requires go 1.13 or later. The Facebook Messenger backend requires Python 3 and the `fbchat` library (tested with v1.9.6).

To build Easybridge, clone this repository outside of your `$GOPATH`.
Then, run `make` in the root of the repo.


## Operating Easybridge

Easybridge acts as a Matrix application service: in this regard,
it requires a registration file to be added to your homeserver.
It uses a database to store configuration and state information,
which can be any backend supported by [Gorm](https://gorm.io).

Easybridge takes a single command line argument, `-config <filename>`, which is the
path to its config file (defaults to `./config.json`).
The configuration file is a JSON file whose contents is described in the following section.

If the config file does not exist, a template will be created.
A template appservice registration file will also be created if necessary.


## Configurating Easybridge

Easybridge is configured using a single JSON configuration file, which contains
a dictionnary whose keys are the following:

- `log_level`: what log level Easybridge runs with (trace, debug, info, warn, error, fatal, panic). **Warning:** in `trace` level, the content of all calls to the Matrix API and some other information will be dumped, exposing user's credentials and messages. In `debug` level, room join/leave information will be exposed. The `info` level (default) does not expose any user's private information.
- `easybridge_avatar`: path to the image that Easybridge uses as an avatar on Matrix

### Matrix configuration

- `registration`: path to the YAML appservice registration file
- `appservice_bind_addr`: on what IP/port to bind as a Matrix app service (HTTP only, no HTTPS)
- `homeserver_url`: HTTP address of the Matrix homeserver
- `matrix_domain`: the domain name of the Matrix homeserver (i.e. the domain used in user identifiers, room identifiers, etc)
- `name_format`: the format of identifiers that are created on Matrix for users and room aliases. `{}` is replaced by the concatenation of user/room identifier and protocol. Typically you want either `_ezbr_{}` or `{}_ezbr`, the latter having the advantage that the briged user's names are then used as prefixes for the created identifiers.

### Web interface configuration

- `web_bind_addr`: on what IP/port to bind for the web interface that allows adding and configuring accounts (HTTP only, no HTTPS, use a reverse proxy for that)
- `web_url`: the outside HTTP/HTTPS address at which the web interface is made available. If set, a widget will be added in the Easybridge room so that users can configure the bridge without leaving the Riot client.
- `session_key`: a key with which session cookies are encrypted for the web interface

### Storage configuration

- `db_type` and `db_path`: the database backend and path to use (see the [Gorm documentation](http://gorm.io/docs/connecting_to_the_database.html))


## Facebook Messenger alternative login procedure

The default login procedure for the Messenger backend is to log in with your email and password.
Unfortunately, if this login procedure happenned to often, you would get rate limited on login
attempts quite rapidly.
To bypass this issue, once sucessfully logged in Easybridge will store a *client pickle* in your account
configuration, i.e. a blob that contains your Facebook cookies so that the username+password procedure
doesn't need to be repeated everytime Easybridge restarts.

If the automated username+password procedure fails, the user can generate a client pickle from the
command line and use that in the configuration instead of entering a username and a password.
To use this method, **do not enter your password in Easybridge when configuring the Messenger backend (only enter your email address)**.
Generate your client pickle by running the following command:

```
./external/messenger.py create_client_pickle
```

This procedure will ask for your email and password and attempt to log you in to Facebook.
If it succeeds, it will print several dozen lines of data looking like `eJyVVlt....X9cgyfgY7mJaK`.
Then, when configuring the Messenger backend in Easybridge,
instead of entering your password,
enter the obtained client pickle string in the appropriate field.

