module git.deuxfleurs.fr/Deuxfleurs/easybridge

go 1.13

require (
	github.com/42wim/matterbridge v1.18.3
	github.com/blang/semver v3.5.1+incompatible // indirect
	github.com/bwmarrin/discordgo v0.20.2 // indirect
	github.com/denisenkom/go-mssqldb v0.0.0-20200910202707-1e08a3fab204 // indirect
	github.com/dfordsoft/golib v0.0.0-20180902042739-76ee6ab99bec // indirect
	github.com/dyatlov/go-opengraph v0.0.0-20180429202543-816b6608b3c8 // indirect
	github.com/go-ldap/ldap v3.0.3+incompatible // indirect
	github.com/go-ldap/ldap/v3 v3.1.7
	github.com/go-redis/redis v6.15.9+incompatible // indirect
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.5-0.20181225215658-ec221ba9ea45+incompatible // indirect
	github.com/google/go-cmp v0.5.2 // indirect
	github.com/google/uuid v1.1.2 // indirect
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/sessions v1.2.1
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/hashicorp/golang-lru v0.5.4
	github.com/jinzhu/gorm v1.9.16
	github.com/jinzhu/now v1.1.1 // indirect
	github.com/kardianos/osext v0.0.0-20170510131534-ae77be60afb1 // indirect
	github.com/kr/pretty v0.2.1 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/labstack/echo v3.3.10+incompatible // indirect
	github.com/lib/pq v1.8.0 // indirect
	github.com/lrstanley/girc v0.0.0-20190801035559-4fc93959e1a7
	github.com/matterbridge/go-xmpp v0.0.0-20200418225040-c8a3a57b4050
	github.com/matterbridge/gomatrix v0.0.0-20191026211822-6fc7accd00ca // indirect
	github.com/mattermost/logr v1.0.13 // indirect
	github.com/mattermost/mattermost-server v5.11.1+incompatible
	github.com/mattermost/mattermost-server/v5 v5.27.0
	github.com/mattn/go-xmpp v0.0.0-20200128155807-a86b6abcb3ad
	github.com/mgutz/ansi v0.0.0-20200706080929-d51e80ef957d // indirect
	github.com/nelsam/hel/v2 v2.3.3 // indirect
	github.com/nicksnyder/go-i18n v1.10.1 // indirect
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/nlopes/slack v0.6.0 // indirect
	github.com/nxadm/tail v1.4.5 // indirect
	github.com/onsi/ginkgo v1.14.1 // indirect
	github.com/onsi/gomega v1.10.2 // indirect
	github.com/pborman/uuid v1.2.1 // indirect
	github.com/pelletier/go-toml v1.8.1 // indirect
	github.com/poy/onpar v1.0.1 // indirect
	github.com/rs/xid v1.2.1
	github.com/shirou/w32 v0.0.0-20160930032740-bb4de0191aa4 // indirect
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/objx v0.3.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	go.uber.org/zap v1.16.0 // indirect
	golang.org/x/crypto v0.0.0-20201002170205-7f63de1d35b0
	golang.org/x/net v0.0.0-20201002202402-0a1ea396d57c // indirect
	golang.org/x/tools v0.0.0-20201002184944-ecd9fd270d5d // indirect
	gopkg.in/asn1-ber.v1 v1.0.0-20181015200546-f715ec2f112d // indirect
	gopkg.in/check.v1 v1.0.0-20200902074654-038fdea0a05b // indirect
	gopkg.in/yaml.v2 v2.3.0
	honnef.co/go/tools v0.0.1-2020.1.5 // indirect
)
