package xmpp

import (
	. "git.deuxfleurs.fr/Deuxfleurs/easybridge/connector"
)

const XMPP_PROTOCOL = "XMPP"

func init() {
	Register(XMPP_PROTOCOL, Protocol{
		NewConnector: func() Connector { return &XMPP{} },
		Schema: ConfigSchema{
			&ConfigEntry{
				Name:        "jid",
				Description: "JID",
				Required:    true,
			},
			&ConfigEntry{
				Name:        "password",
				Description: "Password",
				Required:    true,
				IsPassword:  true,
			},
			&ConfigEntry{
				Name:        "nickname",
				Description: "Nickname in MUCs",
				Required:    true,
			},
			&ConfigEntry{
				Name:        "port",
				Description: "Port",
				IsNumeric:   true,
				Default:     "5222",
			},
			&ConfigEntry{
				Name:        "ssl",
				Description: "Use SSL",
				IsBoolean:   true,
				Default:     "true",
			},
		},
	})
}
