package external

import (
	"os"

	. "git.deuxfleurs.fr/Deuxfleurs/easybridge/connector"
)

const MESSENGER_PROTOCOL = "Messenger"

func init() {
	Register(MESSENGER_PROTOCOL, Protocol{
		NewConnector: func() Connector {
			return &External{
				protocol: MESSENGER_PROTOCOL,
				command:  "./external/messenger.py",
				debug:    (os.Getenv("EASYBRIDGE_MESSENGER_DEBUG") == "true"),
			}
		},
		Schema: ConfigSchema{
			&ConfigEntry{
				Name:        "email",
				Description: "Email address",
				Required:    true,
			},
			&ConfigEntry{
				Name:        "password",
				Description: "Password",
				IsPassword:  true,
			},
			&ConfigEntry{
				Name:        "client_pickle",
				Description: "Client pickle (alternative login method)",
			},
			&ConfigEntry{
				Name:        "initial_backlog",
				Description: "Maximum number of messages to load when joining a channel",
				IsNumeric:   true,
				Default:     "100",
			},
		},
	})
}
