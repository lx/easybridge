package connector

import (
	"fmt"
	"strconv"
	"strings"
)

type Configuration map[string]string

func (c Configuration) GetString(k string, deflt ...string) (string, error) {
	if ss, ok := c[k]; ok {
		return ss, nil
	}
	if len(deflt) > 0 {
		return deflt[0], nil
	}
	return "", fmt.Errorf("Missing configuration key: %s", k)
}

func (c Configuration) GetInt(k string, deflt ...int) (int, error) {
	if ss, ok := c[k]; ok {
		return strconv.Atoi(ss)
	}
	if len(deflt) > 0 {
		return deflt[0], nil
	}
	return 0, fmt.Errorf("Missing configuration key: %s", k)
}

func (c Configuration) GetBool(k string, deflt ...bool) (bool, error) {
	if ss, ok := c[k]; ok {
		if strings.EqualFold(ss, "true") {
			return true, nil
		} else if strings.EqualFold(ss, "false") {
			return false, nil
		} else {
			return false, fmt.Errorf("Invalid value: %s", ss)
		}
	}
	if len(deflt) > 0 {
		return deflt[0], nil
	}
	return false, fmt.Errorf("Missing configuration key: %s", k)
}

// ----

type Protocol struct {
	NewConnector func() Connector
	Schema       ConfigSchema
}

type ConfigSchema []*ConfigEntry

type ConfigEntry struct {
	Name        string
	Description string
	Default     string
	FixedValue  string
	Required    bool
	IsPassword  bool
	IsNumeric   bool
	IsBoolean   bool
}

var Protocols = map[string]Protocol{}

func Register(name string, protocol Protocol) {
	Protocols[name] = protocol
}
