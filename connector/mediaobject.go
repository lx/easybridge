package connector

import (
	"bytes"
	"io"
	"net/http"
	"os"
	"path/filepath"

	log "github.com/sirupsen/logrus"
)

type FileMediaObject struct {
	Path string
}

func (m *FileMediaObject) Filename() string {
	return filepath.Base(m.Path)
}

func (m *FileMediaObject) Size() int64 {
	fi, err := os.Stat(m.Path)
	if err != nil {
		log.Fatal(err)
	}
	return fi.Size()
}

func (m *FileMediaObject) Mimetype() string {
	f, err := os.Open(m.Path)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	buffer := make([]byte, 512)
	_, err = f.Read(buffer)
	if err != nil {
		log.Fatal(err)
	}

	return http.DetectContentType(buffer)
}

func (m *FileMediaObject) ImageSize() *ImageSize {
	// TODO but not really usefull
	return nil
}

func (m *FileMediaObject) Read() (io.ReadCloser, error) {
	return os.Open(m.Path)
}

func (m *FileMediaObject) URL() string {
	return ""
}

// ----

type BlobMediaObject struct {
	ObjectFilename  string
	ObjectMimetype  string
	ObjectImageSize *ImageSize
	ObjectData      []byte
}

func (m *BlobMediaObject) Filename() string {
	return m.ObjectFilename
}

func (m *BlobMediaObject) Size() int64 {
	return int64(len(m.ObjectData))
}

func (m *BlobMediaObject) Mimetype() string {
	return m.ObjectMimetype
}

func (m *BlobMediaObject) ImageSize() *ImageSize {
	return m.ObjectImageSize
}

func (m *BlobMediaObject) Read() (io.ReadCloser, error) {
	return nullCloseReader{bytes.NewBuffer(m.ObjectData)}, nil
}

func (m *BlobMediaObject) URL() string {
	return ""
}

type nullCloseReader struct {
	io.Reader
}

func (ncr nullCloseReader) Close() error {
	return nil
}

// ----

type LazyBlobMediaObject struct {
	ObjectFilename  string
	ObjectMimetype  string
	ObjectImageSize *ImageSize
	ObjectData      []byte

	GetFn func(o *LazyBlobMediaObject) error
}

func (m *LazyBlobMediaObject) Filename() string {
	return m.ObjectFilename
}

func (m *LazyBlobMediaObject) Size() int64 {
	if m.ObjectData == nil {
		m.GetFn(m)
	}
	return int64(len(m.ObjectData))
}

func (m *LazyBlobMediaObject) Mimetype() string {
	if m.ObjectData == nil {
		m.GetFn(m)
	}
	return m.ObjectMimetype
}

func (m *LazyBlobMediaObject) ImageSize() *ImageSize {
	if m.ObjectData == nil {
		m.GetFn(m)
	}
	return m.ObjectImageSize
}

func (m *LazyBlobMediaObject) Read() (io.ReadCloser, error) {
	if m.ObjectData == nil {
		err := m.GetFn(m)
		if err != nil {
			return nil, err
		}
	}
	return nullCloseReader{bytes.NewBuffer(m.ObjectData)}, nil
}

func (m *LazyBlobMediaObject) URL() string {
	return ""
}
