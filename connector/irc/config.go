package irc

import (
	. "git.deuxfleurs.fr/Deuxfleurs/easybridge/connector"
)

const IRC_PROTOCOL = "IRC"

func init() {
	Register(IRC_PROTOCOL, Protocol{
		NewConnector: func() Connector { return &IRC{} },
		Schema: ConfigSchema{
			&ConfigEntry{
				Name:        "nick",
				Description: "Nickname",
				Required:    true,
			},
			&ConfigEntry{
				Name:        "server",
				Description: "Server",
				Required:    true,
			},
			&ConfigEntry{
				Name:        "port",
				Description: "Port",
				IsNumeric:   true,
				Default:     "6667",
			},
			&ConfigEntry{
				Name:        "ssl",
				Description: "Use SSL",
				IsBoolean:   true,
				Default:     "false",
			},
			&ConfigEntry{
				Name:        "server_pass",
				Description: "Server password (authenticate with PASS command)",
				IsPassword:  true,
			},
			&ConfigEntry{
				Name:        "sasl_user",
				Description: "Username for SASL authentication",
			},
			&ConfigEntry{
				Name:        "sasl_pass",
				Description: "Password for SASL authentication",
				IsPassword:  true,
			},
		},
	})
}
