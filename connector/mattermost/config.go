package mattermost

import (
	. "git.deuxfleurs.fr/Deuxfleurs/easybridge/connector"
)

const MATTERMOST_PROTOCOL = "Mattermost"

func init() {
	Register(MATTERMOST_PROTOCOL, Protocol{
		NewConnector: func() Connector { return &Mattermost{} },
		Schema: ConfigSchema{
			&ConfigEntry{
				Name:        "server",
				Description: "Server",
				Required:    true,
			},
			&ConfigEntry{
				Name:        "username",
				Description: "Username",
				Required:    true,
			},
			&ConfigEntry{
				Name:        "password",
				Description: "Password",
				IsPassword:  true,
			},
			&ConfigEntry{
				Name:        "token",
				Description: "Authentification token (replaces password if set)",
			},
			&ConfigEntry{
				Name:        "teams",
				Description: "Comma-separated list of teams to follow",
				Required:    true,
			},
			&ConfigEntry{
				Name:        "no_tls",
				Description: "Disable SSL/TLS",
				IsBoolean:   true,
				Default:     "false",
			},
			&ConfigEntry{
				Name:        "initial_backlog",
				Description: "Maximum number of messages to load when joining a channel",
				IsNumeric:   true,
				Default:     "1000",
			},
			&ConfigEntry{
				Name:        "initial_members",
				Description: "Maximum number of members to load when joining a channel",
				IsNumeric:   true,
				Default:     "100",
			},
		},
	})
}
